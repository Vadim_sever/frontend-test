module.exports = function(grunt) {
    'use strict';
    var debug;

    require('load-grunt-tasks')(grunt);
    debug = !!grunt.option('debug');
    
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        banner: '/* Authors: <%= pkg.authors %>, <%= grunt.template.today("yyyy-mm-dd") %> */\n',

        clean: {
            all: {
                src: [ 'css/_*.css', 'lib/_*.js' ]
            },
            css: {
                src: [ 'css/_*.css' ]
            },
            js: {
                src: [ 'lib/_*.js' ]
            }
        },

        stylus: {
            options: {
                banner: '<%= banner %>',
                compress: false
            },

            main: {
                files: {
                    'css/_.css': 'styl/_.styl'
                }
            }
        },

        autoprefixer: {
            options: {
                cascade: true,
                browsers: ['last 6 versions', 'ie 7', 'ie 8', 'ie 9']
            },
            files: {
                flatten: true,
                src: 'css/*.css'
            }
        },

        // COMPRESSORS
        cssmin: {
            main: {
                files: {
                    'css/_.min.css': [
                        'css/vendor/**/*.css',
                        'css/*.css',
                        '!css/*.min.css'
                    ]
                }
            }
        },

        uglify: {
            build: {
                options: {
                    mangle: false
                },
                files: {
                    'lib/_.min.js': ['lib/*.js', '!lib/main.js', 'lib/main.js' ]
                }
            }
        },

        connect: {
            server: {
                options: {
                    port: 9090,
                    base: '.',
                    hostname: '*',
                    onCreateServer: function(server, connect, options) {
                        var io = require('socket.io').listen(server);
                        io.sockets.on('connection', function(socket) {
                            // do something with socket
                        });
                    }
                }
            }
        },

        watch: {
            options: {
                livereload: true
            },
            html: {
                files: ['*.html'],
                tasks: ['dohtml']
            },
            stylus: {
                files: 'styl/**/*.styl',
                tasks: ['dostyl']
            },
            css: {
                files: ['css/vendor/**/*.css'],
                tasks: ['docss']
            },
            js: {
                files: ['lib/**/*.js'],
                tasks: ['dojs']
            }
        }

    });

    grunt.registerTask(
        'dohtml',
        []
    );

    grunt.registerTask(
        'docss',
        [ 'clean:css', 'autoprefixer', 'cssmin' ]
    );

    grunt.registerTask(
        'dostyl',
        [ 'clean:css', 'stylus', 'autoprefixer', 'cssmin' ]
    );

    grunt.registerTask(
        'dojs',
        [ 'clean:js', 'uglify' ]
    );

    return grunt.registerTask(
        'default',
        [ 'dohtml', 'dostyl', 'dojs', 'connect:server', 'watch' ]
    );
};
